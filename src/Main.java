/**
 * Created by ITA on 23.10.2014.
 */
public class Main {

    public static void main(String... args) {
        Dog dog = new Dog();
        Dog dog1 = new Dog("Собака", "гав-гав");
        Dog dog2 = new Dog("Тузик", "тяф-тяф");
        Lemur defaultLemur = new Lemur();
        Lemur smartLemur = new Lemur("Умный лемур", "Я умный!");


        dog.printDisplay();
        dog1.printDisplay();
        dog2.printDisplay();
        defaultLemur.printDisplay();
        smartLemur.printDisplay();
    }
}
