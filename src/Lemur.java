/**
 * Created by Кирилл on 02.02.2017.
 */
public class Lemur extends Animal {

    public Lemur(String name, String voice) {
        super(name, voice);
    }

    public Lemur() {
        this("Лемур", "Я умнее человека");
    }

}
