/**
 * Класс собаки
 * @author Малякин Кирилл. гр. 15ОИТ20
 */
public class Dog extends Animal {


    public Dog() {
        this("Собака", "гав-гав");
    }


    protected Dog(String name, String voice) {
        super(name, voice);
    }

}
